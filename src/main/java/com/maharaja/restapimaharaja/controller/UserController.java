package com.maharaja.restapimaharaja.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maharaja.restapimaharaja.dto.LoginDTO;
import com.maharaja.restapimaharaja.dto.UserDTO;
import com.maharaja.restapimaharaja.service.UserService;
import com.maharaja.restapimaharaja.util.AppConstant;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api/v1/service")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@PostMapping(value = "/register")
	public ResponseEntity<Object> registerUser(@RequestBody UserDTO userDTO) {
		String resp = userService.registerUser(userDTO);
		if(resp.equals(AppConstant.OK)) {
			return new ResponseEntity<Object>("Successfully Saved", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(value = "/login")
	public ResponseEntity<Object> registerUser(@RequestBody LoginDTO loginDTO) {
		String resp = userService.login(loginDTO);
		if(resp.equals(AppConstant.OK)) {
			return new ResponseEntity<Object>("Successfully Saved", HttpStatus.OK);
		} else {
			return new ResponseEntity<Object>(resp, HttpStatus.OK);
		}
	}
	
	@GetMapping
	public String test() {
		return "work";
	}

}
