package com.maharaja.restapimaharaja.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.maharaja.restapimaharaja.dto.ProgramDTO;
import com.maharaja.restapimaharaja.service.ProgramService;
import com.maharaja.restapimaharaja.util.AppConstant;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/content/programs")
public class ProgramController {

	@Autowired
	private ProgramService programService;

	@PostMapping("/create")
	public ResponseEntity<Object> createProgram(@RequestBody ProgramDTO programDTO,
			@RequestHeader("Authorization") String token) {
		try {
			String resp = programService.addProgram(programDTO, token);
			if (resp.equals(AppConstant.OK)) {
				return new ResponseEntity<Object>("Successfully Saved", HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/update")
	public ResponseEntity<Object> updateProgram(@RequestBody ProgramDTO programDTO,
			@RequestHeader("Authorization") String token) {
		try {
			String resp = programService.addProgram(programDTO, token);
			if (resp.equals(AppConstant.OK)) {
				return new ResponseEntity<Object>("Successfully Updated", HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping
	public ResponseEntity<Object> getProgram() {
		try {
			List<ProgramDTO> programDTOs = programService.getAllProgram();

			return new ResponseEntity<Object>(programDTOs, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
