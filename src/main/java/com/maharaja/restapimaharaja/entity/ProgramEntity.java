package com.maharaja.restapimaharaja.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "program")
public class ProgramEntity {

	private Integer programId;
	private String name;
	private String description;
	private Date startDate;
	private Date endDate;

	private UserEntity createBy;
	private Date createDate;
	private UserEntity modifyBy;
	private Date modifyDate;
	private String status;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getProgramId() {
		return programId;
	}

	public void setProgramId(Integer programId) {
		this.programId = programId;
	}

	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "start_date", nullable = false, columnDefinition = "DATE")
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Column(name = "end_date", nullable = false, columnDefinition = "DATE")
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "create_by", nullable = false)
	public UserEntity getCreateBy() {
		return createBy;
	}

	public void setCreateBy(UserEntity createBy) {
		this.createBy = createBy;
	}

	@Column(name = "create_date", nullable = false)
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "modify_by")
	public UserEntity getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(UserEntity modifyBy) {
		this.modifyBy = modifyBy;
	}

	@Column(name = "modify_date")
	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	@Column(name = "status", nullable = false)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
