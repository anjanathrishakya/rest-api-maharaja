package com.maharaja.restapimaharaja.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "sys_user")
public class UserEntity {

	private Integer id;
	private String username;
	private String password;
	private String name;
	private String mobileNumber;
	private Date dateOfBirth;
	private String gender;
	private String language;
	private String status;
	
	private List<ProgramEntity> createProgramEntities;
	private List<ProgramEntity> modifyProgramEntities;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "username", nullable = false)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "password", nullable = false)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "mobile_number", nullable = false, length = 15)
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Column(name = "date_of_birth", nullable = false, columnDefinition = "DATE")
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@Column(name = "gender", nullable = false, length = 1)
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column(name = "language", nullable = false, length = 5)
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@Column(name = "status", nullable = false, length = 10)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@OneToMany(mappedBy = "createBy", targetEntity = ProgramEntity.class)
	public List<ProgramEntity> getCreateProgramEntities() {
		return createProgramEntities;
	}

	public void setCreateProgramEntities(List<ProgramEntity> createProgramEntities) {
		this.createProgramEntities = createProgramEntities;
	}

	@OneToMany(mappedBy = "modifyBy", targetEntity = ProgramEntity.class)
	public List<ProgramEntity> getModifyProgramEntities() {
		return modifyProgramEntities;
	}

	public void setModifyProgramEntities(List<ProgramEntity> modifyProgramEntities) {
		this.modifyProgramEntities = modifyProgramEntities;
	}
}
