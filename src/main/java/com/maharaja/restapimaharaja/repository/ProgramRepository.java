package com.maharaja.restapimaharaja.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.maharaja.restapimaharaja.entity.ProgramEntity;

public interface ProgramRepository extends CrudRepository<ProgramEntity, Integer>{

	ProgramEntity findOneByProgramId(Integer program_id);

	List<ProgramEntity> findByStatus(String active);

}
