package com.maharaja.restapimaharaja.repository;

import org.springframework.data.repository.CrudRepository;

import com.maharaja.restapimaharaja.entity.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {

	UserEntity findOneByUsername(String username) throws Exception;

	UserEntity findOneByUsernameAndPasswordAndStatus(String username, String password, String status) throws Exception;

	UserEntity findOneById(Integer userId) throws Exception;

}
