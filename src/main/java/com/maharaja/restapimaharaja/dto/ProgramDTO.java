package com.maharaja.restapimaharaja.dto;

import java.util.Date;

public class ProgramDTO {

	private Integer program_id;
	private String name;
	private String description;
	private Date start_date;
	private Date end_date;

	public Integer getProgram_id() {
		return program_id;
	}

	public void setProgram_id(Integer program_id) {
		this.program_id = program_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	@Override
	public String toString() {
		return "ProgramDTO [program_id=" + program_id + ", name=" + name + ", description=" + description
				+ ", start_date=" + start_date + ", end_date=" + end_date + "]";
	}

}
