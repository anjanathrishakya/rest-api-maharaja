package com.maharaja.restapimaharaja.dto;

import java.security.Key;
import java.util.Date;

public class AuthDto {
	private Key secretKey;
	private Date expireDate;

	public Key getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(Key secretKey) {
		this.secretKey = secretKey;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}
}
