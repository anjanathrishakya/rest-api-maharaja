package com.maharaja.restapimaharaja.service;

import java.text.ParseException;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.maharaja.restapimaharaja.dto.LoginDTO;
import com.maharaja.restapimaharaja.dto.UserDTO;
import com.maharaja.restapimaharaja.entity.UserEntity;

public interface UserService extends UserDetailsService{
	
	String registerUser(UserDTO userDto) ;
	
	String login(LoginDTO loginDto) ;

	UserEntity getUserEntity(UserDTO userDto) throws ParseException;
	
	public void authenticateUserTokenExpire()throws Exception;

}
