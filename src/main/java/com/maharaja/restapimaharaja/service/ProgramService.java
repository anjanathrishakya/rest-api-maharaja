package com.maharaja.restapimaharaja.service;

import java.util.List;

import com.maharaja.restapimaharaja.dto.ProgramDTO;
import com.maharaja.restapimaharaja.entity.ProgramEntity;
import com.maharaja.restapimaharaja.entity.UserEntity;

public interface ProgramService {
	
	String addProgram(ProgramDTO programDTO, String token);
	
	List<ProgramDTO> getAllProgram();

	ProgramEntity getProgramEntity(ProgramDTO programDTO, UserEntity userEntity);

	ProgramDTO getProgramDTO(ProgramEntity entity);

	
	

}
