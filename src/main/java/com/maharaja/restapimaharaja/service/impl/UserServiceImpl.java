package com.maharaja.restapimaharaja.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maharaja.restapimaharaja.config.JwtTokenProvider;
import com.maharaja.restapimaharaja.config.ReserveKey;
import com.maharaja.restapimaharaja.dto.AuthDto;
import com.maharaja.restapimaharaja.dto.LoginDTO;
import com.maharaja.restapimaharaja.dto.UserDTO;
import com.maharaja.restapimaharaja.entity.UserEntity;
import com.maharaja.restapimaharaja.repository.UserRepository;
import com.maharaja.restapimaharaja.service.UserService;
import com.maharaja.restapimaharaja.util.AppConstant;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ReserveKey reserveKey;
	
	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Override
	public String registerUser(UserDTO userDto) {

		UserEntity userEntity = null;
		try {
			userEntity = getUserEntity(userDto);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}

		if (userRepository.save(userEntity) != null) {
			return AppConstant.OK;
		} else {
			return AppConstant.FAIL;
		}

	}

	@Override
	public UserEntity getUserEntity(UserDTO userDto) throws ParseException {
		UserEntity userEntity = new UserEntity();
		userEntity.setDateOfBirth(new SimpleDateFormat("yyyy-MM-dd").parse(userDto.getDate_of_birth()));
		userEntity.setGender(userDto.getGender());
		userEntity.setLanguage(userDto.getLanguage());
		userEntity.setMobileNumber(userDto.getMobile_number());
		userEntity.setName(userDto.getName());
		userEntity.setPassword(userDto.getPassword());
		userEntity.setStatus(AppConstant.ACTIVE);
		userEntity.setUsername(userDto.getUsername());
		return userEntity;
	}

	@Override
	public String login(LoginDTO loginDto) {
		UserEntity userEntity = null;
		try {
			userEntity = userRepository.findOneByUsernameAndPasswordAndStatus(loginDto.getUsername(),
					loginDto.getPassword(), AppConstant.ACTIVE);
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
		if (userEntity != null) {
			String token = jwtTokenProvider.createToken(userEntity.getUsername(), userEntity.getId());
			return token;
		}
		return "invalid";
	}

	@Override
	@Scheduled(fixedRateString = "${fixedRate.in.milliseconds}")
	public void authenticateUserTokenExpire() throws Exception {

		HashMap<String, AuthDto> loginedUsers = reserveKey.getAllLoginUser();

		if (!loginedUsers.isEmpty()) {
			for (Map.Entry<String, AuthDto> authUser : loginedUsers.entrySet()) {
				if (authUser.getValue().getExpireDate().before(new Date())) {
					reserveKey.removeKey(authUser.getKey());
				}
			}
		}

	}

	@Override
	public UserDetails loadUserByUsername(String username) {

		UserEntity userEntity = null;
		try {
			userEntity = userRepository.findOneByUsername(username);
			ArrayList<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
			return new User(userEntity.getUsername(), userEntity.getPassword(), roles);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}
