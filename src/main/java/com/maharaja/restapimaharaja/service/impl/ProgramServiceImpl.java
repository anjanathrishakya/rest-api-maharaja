package com.maharaja.restapimaharaja.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.maharaja.restapimaharaja.config.JwtTokenProvider;
import com.maharaja.restapimaharaja.dto.ProgramDTO;
import com.maharaja.restapimaharaja.entity.ProgramEntity;
import com.maharaja.restapimaharaja.entity.UserEntity;
import com.maharaja.restapimaharaja.repository.ProgramRepository;
import com.maharaja.restapimaharaja.repository.UserRepository;
import com.maharaja.restapimaharaja.service.ProgramService;
import com.maharaja.restapimaharaja.util.AppConstant;
import com.maharaja.restapimaharaja.validation.ProgramValidation;

@Service
@Transactional
public class ProgramServiceImpl implements ProgramService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ProgramRepository programRepository;

	@Autowired
	private ProgramValidation programValidation;
	
	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Override
	public String addProgram(ProgramDTO programDTO, String token) {

		UserEntity userEntity = null;
		try {
			String validateResp = programValidation.validateProgramDTO(programDTO);
			if (!(validateResp.equals(AppConstant.VALID))) {
				return validateResp;
			}

			userEntity = userRepository.findOneById(jwtTokenProvider.getUserid(token));
		} catch (Exception e) {
			e.printStackTrace();
			return e.getLocalizedMessage();
		}

		if (userEntity == null) {
			return "User Not Found";
		}

		ProgramEntity programEntity = getProgramEntity(programDTO, userEntity);

		if (programRepository.save(programEntity) != null) {
			return AppConstant.OK;
		} else {
			return "Fail at Program Saving";
		}
	}

	@Override
	public List<ProgramDTO> getAllProgram() {
		List<ProgramEntity> programEntities = programRepository.findByStatus(AppConstant.ACTIVE);
		List<ProgramDTO> programDTOs = new ArrayList<ProgramDTO>();

		programEntities.forEach(entity -> {
			programDTOs.add(getProgramDTO(entity));
		});
		return programDTOs;
	}

	@Override
	public ProgramEntity getProgramEntity(ProgramDTO programDTO, UserEntity userEntity) {
		ProgramEntity programEntity = null;

		if (programDTO.getProgram_id() != null) {
			programEntity = programRepository.findOneByProgramId(programDTO.getProgram_id());
		}
		if (programEntity == null) {
			programEntity = new ProgramEntity();

			programEntity.setCreateBy(userEntity);
			programEntity.setCreateDate(new Date());
		} else {
			programEntity.setModifyBy(userEntity);
			programEntity.setModifyDate(new Date());
		}

		programEntity.setDescription(programDTO.getDescription());
		programEntity.setEndDate(programDTO.getEnd_date());
		programEntity.setStartDate(programDTO.getStart_date());
		programEntity.setName(programDTO.getName());
		programEntity.setStatus(AppConstant.ACTIVE);

		return programEntity;
	}

	@Override
	public ProgramDTO getProgramDTO(ProgramEntity entity) {
		ProgramDTO dto = new ProgramDTO();
		dto.setDescription(entity.getDescription());
		dto.setEnd_date(entity.getEndDate());
		dto.setName(entity.getName());
		dto.setProgram_id(entity.getProgramId());
		dto.setStart_date(entity.getStartDate());

		return dto;
	}

}
