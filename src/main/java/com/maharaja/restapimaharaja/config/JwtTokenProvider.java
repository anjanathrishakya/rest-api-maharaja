package com.maharaja.restapimaharaja.config;

import java.security.Key;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.maharaja.restapimaharaja.dto.AuthDto;
import com.maharaja.restapimaharaja.service.UserService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

@Component
public class JwtTokenProvider {

	@Autowired
	private ReserveKey reserveKey;

	@Autowired
	private UserService userService;

	@Autowired
	private AuthSiginKeyResolver authSiginKeyResolver;

	@Value("${expireTime}")
	private String expireTime;

	public String createToken(String username, Integer uid) {

		Key mainKey = Keys.secretKeyFor(SignatureAlgorithm.HS256);

		System.out.println(expireTime);
		AuthDto authDto = new AuthDto();
		authDto.setSecretKey(mainKey);

		authDto.setExpireDate(getExpireDate(Integer.parseInt(expireTime)));

		boolean result = reserveKey.addKey(username, authDto);
		if (result) {
			Claims claims = Jwts.claims().setSubject(username);
			claims = Jwts.claims().setSubject(username);
			claims.put("userid", uid);
			claims.put("username", username);
			claims.put("main key", mainKey.toString());

			return Jwts.builder().setClaims(claims).setHeaderParam("keyId", mainKey).signWith(mainKey).compact();
		} else {
			reserveKey.updateKey(username, authDto);
			Claims claims = Jwts.claims().setSubject(username);
			claims.put("userid", uid);
			claims.put("username", username);
			claims.put("main key", mainKey.toString());

			return Jwts.builder().setClaims(claims).setHeaderParam("keyId", mainKey).signWith(mainKey).compact();
		}

	}

	public boolean validateToken(String token) {
		try {
			Claims claims = Jwts.parser().setSigningKeyResolver(authSiginKeyResolver).parseClaimsJws(token).getBody();

			if (claims != null) {

				AuthDto authDto = reserveKey.getKey(claims.getSubject());
				if (authDto != null) {

					authDto.setExpireDate(getExpireDate(Integer.parseInt(expireTime)));
					reserveKey.updateKey(claims.getSubject(), authDto);
					return true;
				}

			}
			return false;
		} catch (Exception e) {
			return false;
		}

	}

	public String getUsername(String token) {
		Jws<Claims> jwsClaims = Jwts.parser().setSigningKeyResolver(authSiginKeyResolver).parseClaimsJws(token);
		;
		return (String) jwsClaims.getBody().getSubject();
	}

	public Integer getUserid(String token) throws Exception {
		if (token != null && token.startsWith("Bearer ")) {
			token = token.substring(7, token.length());
		}
		Claims claims = getClaims(token);
		return (Integer) claims.get("userid");
	}

	public Claims getClaims(String token) {
		return Jwts.parser().setSigningKeyResolver(authSiginKeyResolver).parseClaimsJws(token).getBody();
	}

	public String resolveToken(HttpServletRequest req) throws ServletException {

		String bearerToken = req.getHeader("Authorization");
		if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

	public Authentication getAuthentication(String token) {
		UserDetails userDetails = this.userService.loadUserByUsername(getUsername(token));
		return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());

	}

	public boolean logOut(String token) throws Exception {
		boolean remove = reserveKey.removeKey(getClaims(token).getSubject());
		if (remove) {
			return true;
		}
		return false;
	}

	public Date getExpireDate(int expiredTimeDif) {

		Calendar calendarTime = Calendar.getInstance();
		calendarTime.setTime(new Date());
		calendarTime.add(Calendar.MINUTE, expiredTimeDif);

		return calendarTime.getTime();
	}

}
