package com.maharaja.restapimaharaja.config;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.DatatypeConverter;

import org.springframework.stereotype.Component;
@Component
public class EncryptData {

	public static String encrypt(String password) throws NoSuchAlgorithmException {
		
		MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(password.getBytes());
	    byte[] digest = md.digest();
	    return DatatypeConverter
	      .printHexBinary(digest).toLowerCase();
	    
	}
}
