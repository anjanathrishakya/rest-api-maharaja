package com.maharaja.restapimaharaja.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class webConfigure implements WebMvcConfigurer {

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedMethods("OPTIONS","GET", "POST", "PUT", "DELETE").allowedOrigins("*")
        .allowedHeaders("*");
	}

		
}
