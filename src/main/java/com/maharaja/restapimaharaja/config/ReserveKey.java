package com.maharaja.restapimaharaja.config;

import java.util.HashMap;

import org.springframework.stereotype.Component;

import com.maharaja.restapimaharaja.dto.AuthDto;

@Component
public class ReserveKey {

	private static HashMap<String, AuthDto> keyList=new HashMap<String, AuthDto>();

	public boolean addKey(String username,AuthDto authDto) {
		if(!keyList.containsKey(username)) {
			keyList.put(username, authDto);
			return true;
		}
		return false;
	}
	

	public AuthDto getKey(String username) {
	
		if(keyList.containsKey(username)) {
			return keyList.get(username);
		}else {
			return null;
		}
		
	}
	

	public boolean removeKey(String username) {
		if(keyList.containsKey(username)) {
			keyList.remove(username);
			return true;
		}
		return false;
	}
	
	public boolean updateKey(String username,AuthDto authDto) {
		if(keyList.containsKey(username)) {
			keyList.replace(username, authDto);
			return true;
		}
		return false;
	}

	public HashMap<String, AuthDto> getAllLoginUser(){
		return keyList;
	}
}
