package com.maharaja.restapimaharaja.config;


import java.security.Key;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.maharaja.restapimaharaja.dto.AuthDto;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.SigningKeyResolverAdapter;

@Component
public class AuthSiginKeyResolver extends SigningKeyResolverAdapter {

	@Autowired
	private ReserveKey reserveKey;
	
	@Override
	public Key resolveSigningKey(@SuppressWarnings("rawtypes") JwsHeader header, Claims claims) {
		AuthDto authDto=reserveKey.getKey((String) claims.get("username"));
		
		if(authDto==null) {
			return null;
		}
		
		if (authDto.getSecretKey() == null ) {			
				return null;
	        }else {
	        	
	        	if(authDto.getExpireDate().before(new Date())) {
	        	
	    			return null;
	        }
			
		}
			
		return authDto.getSecretKey();
		
		
	}

	

}
