package com.maharaja.restapimaharaja.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;


@Configuration
@EnableWebSecurity

public class SecurityConfiguration extends WebSecurityConfigurerAdapter{

	
	@Autowired
	private JwtTokenProvider jwtTokenProvider;
	
	


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.cors()
        .and()
        .csrf()
        .disable()
        .httpBasic().disable()
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
            .authorizeRequests()
            .antMatchers(HttpMethod.OPTIONS).permitAll()
            .antMatchers(HttpMethod.POST).permitAll()
            .antMatchers(HttpMethod.GET).permitAll()
            .antMatchers(HttpMethod.DELETE).permitAll()
            .antMatchers(HttpMethod.PUT).permitAll()
            .anyRequest().authenticated()
            .and()
            .apply(new JwtConfigurer(jwtTokenProvider));

	}
	
	
}
