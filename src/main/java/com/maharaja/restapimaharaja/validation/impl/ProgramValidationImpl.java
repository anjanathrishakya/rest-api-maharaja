package com.maharaja.restapimaharaja.validation.impl;

import org.springframework.stereotype.Component;

import com.maharaja.restapimaharaja.dto.ProgramDTO;
import com.maharaja.restapimaharaja.util.AppConstant;
import com.maharaja.restapimaharaja.validation.ProgramValidation;

@Component
public class ProgramValidationImpl implements ProgramValidation {

	@Override
	public String validateProgramDTO(ProgramDTO programDTO) throws Exception {
		if (!(programDTO.getDescription() != null && !programDTO.getDescription().isEmpty()
				&& programDTO.getName() != null && !programDTO.getName().isEmpty() && programDTO.getStart_date() != null
				&& programDTO.getEnd_date() != null)) {
			return "Name, Description, StartDate and End Date can't be null";
		}
		if (programDTO.getStart_date().getTime() > programDTO.getEnd_date().getTime()) {
			return "End Date must be greater than Start Date";
		}
		return AppConstant.VALID;
	}

}
