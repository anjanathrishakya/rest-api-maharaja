package com.maharaja.restapimaharaja.validation;

import com.maharaja.restapimaharaja.dto.ProgramDTO;

public interface ProgramValidation {

	String validateProgramDTO(ProgramDTO programDTO) throws Exception;

}
